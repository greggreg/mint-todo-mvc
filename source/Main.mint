component Main {
  connect Todo.Store exposing { todosToView }
  state editing : Maybe(Number) = Maybe::Nothing
  state newTodoText = ""

  style atLeast1Todo {
    if (Array.isEmpty(Todo.Store.todos)) {
      display: none;
    } else {
      display: block;
    }
  }

  /* event handlers */
  fun updateNewTodoText (event : Html.Event) : Promise(Never, Void) {
    next { newTodoText = Dom.getValue(event.target) }
  }

  fun handleKeyDown (event : Html.Event) : a {
    case (event.keyCode) {
      /* enter */
      13 =>
        sequence {
          Html.Event.preventDefault(event)
          Todo.Store.addNewTodo(newTodoText)
          next { newTodoText = "" }
        }

      => Promise.never()
    }
  }

  fun onEditStart (id : Number, event : Html.Event) : Promise(Never, Void) {
    next { editing = Maybe::Just(id) }
  }

  fun onEditStop (event : Html.Event) : Promise(Never, Void) {
    next { editing = Maybe::Nothing }
  }

  fun handleClearCompleted (event : Html.Event) : a {
    Todo.Store.clearCompleted()
  }

  fun handleToggleAll (event : Html.Event) : a {
    Todo.Store.toggleAll()
  }

  /* class for filter toggle */
  fun selectedIfViewing (view : TodoFilter) : String {
    if (Todo.Store.todoFilter == view) {
      "selected"
    } else {
      ""
    }
  }

  fun render : Html {
    <div class="learn-bar">
      <Sidebar/>

      <div class="todomvc-wrapper">
        <section class="todoapp">
          <header class="header">
            <h1>"todos"</h1>

            <input
              class="new-todo"
              placeholder="What needs to be done?"
              autofocus=""
              value={newTodoText}
              onChange={updateNewTodoText}
              onKeyDown={handleKeyDown}
              name="newTodo"/>
          </header>

          <section class="main">
            <input
              class="toggle-all"
              checked={Todo.Store.todosActive == 0}
              id="toggle-all"
              type="checkbox"
              onClick={handleToggleAll}
              name="toggle"/>

            <label::atLeast1Todo for="toggle-all">
              "Mark all as complete"
            </label>

            /* - Todo List - */
            <ul class="todo-list">
              <{ todoViews }>
            </ul>

            /* - End Todo List - */
          </section>

          <footer::atLeast1Todo class="footer">
            <span class="todo-count">
              <strong>"#{Todo.Store.todosActive} "</strong>
              "items left"
            </span>

            <ul class="filters">
              <li>
                <a
                  class={selectedIfViewing(TodoFilter::All)}
                  href="/#/">

                  "All"

                </a>
              </li>

              <li>
                <a
                  class={selectedIfViewing(TodoFilter::Active)}
                  href="/#/active">

                  "Active"

                </a>
              </li>

              <li>
                <a
                  class={selectedIfViewing(TodoFilter::Completed)}
                  href="/#/completed">

                  "Completed"

                </a>
              </li>
            </ul>

            <button
              class="clear-completed"
              onClick={handleClearCompleted}>

              "Clear completed (#{Todo.Store.todosCompleted})"

            </button>
          </footer>
        </section>

        <footer class="info">
          <p>"Double-click to edit a todo"</p>

          <p>
            "Written by "

            <a href="https://gitlab.com/greggreg">
              "Greg Greg"
            </a>
          </p>

          <p>
            "Part of "

            <a href="http://todomvc.com">
              "TodoMVC"
            </a>
          </p>
        </footer>
      </div>
    </div>
  } where {
    todoViews =
      for (todo of Array.reverse(todosToView)) {
        if (editing == Maybe::Just(todo.id)) {
          <Todo.View.Edit
            key={Number.toString(todo.id)}
            todo={todo}
            onEditStop={onEditStop}/>
        } else {
          <Todo.View.Show
            key={Number.toString(todo.id)}
            todo={todo}
            onEditStart={onEditStart}/>
        }
      }
  }
}

/* This is the Show View for a single Todo */
component Todo.View.Show {
  property todo : Todo
  property onEditStart : Function(Number, Html.Event, Promise(Never, Void))

  connect Todo.Store exposing { updateTodo }

  fun handleCompletedToggle (e : Html.Event) : Promise(Never, Void) {
    updateTodo({ todo | completed = !todo.completed })
  }

  fun handleDestroyClick (id : Number, event : Html.Event) : a {
    Todo.Store.destroy(id)
  }

  fun render : Html {
    <li
      class={
        if (todo.completed) {
          "completed"
        } else {
          ""
        }
      }>

      <div class="view">
        <input
          class="toggle"
          type="checkbox"
          checked={todo.completed}
          onChange={handleCompletedToggle}/>

        <label onDoubleClick={onEditStart(todo.id)}>
          <{ todo.label }>
        </label>

        <button
          class="destroy"
          onClick={handleDestroyClick(todo.id)}/>
      </div>

      <input
        class="edit"
        name="title"
        id="todo-0"/>

    </li>
  }
}

/* This is the Edit View of a single Todo */
component Todo.View.Edit {
  property todo : Todo
  property onEditStop : Function(Html.Event, Promise(Never, Void))
  state todoId = -1
  state todoLabel = ""

  fun componentDidMount : Promise(Never, Void) {
    sequence {
      `document.getElementById("todo-edit-input").focus()`

      next
        {
          todoLabel = todo.label,
          todoId = todo.id
        }
    }
  }

  /* Event Handlers */
  fun updateTodoLabel (event : Html.Event) : a {
    if (todoId != todo.id) {
      componentDidMount()
    } else {
      next { todoLabel = Dom.getValue(event.target) }
    }
  }

  fun handleKeyDown (event : Html.Event) : a {
    case (event.keyCode) {
      /* enter */
      13 =>
        sequence {
          Html.Event.preventDefault(event)
          Todo.Store.updateTodo({ todo | label = todoLabel })
          onEditStop(event)
        }

      /* escape */
      27 =>
        sequence {
          Html.Event.preventDefault(event)
          onEditStop(event)
        }

      => Promise.never()
    }
  }

  fun render : Html {
    <li
      class={
        if (todo.completed) {
          "completed editing"
        } else {
          "editing"
        }
      }>

      <input
        class="edit"
        name="title"
        id="todo-edit-input"
        value={todoLabel}
        onKeyDown={handleKeyDown}
        onChange={updateTodoLabel}/>

    </li>
  }
}

/* This is the internal representation of a Todo */
record Todo {
  label : String,
  completed : Bool,
  id : Number
}

/* This control what filter we are currently viewing */
enum TodoFilter {
  All
  Active
  Completed
}

/* This is the store for Todos */
store Todo.Store {
  const LOCAL_STORAGE_KEY = "mint-todo-todos"
  state todos : Array(Todo) = []
  state todoFilter : TodoFilter = TodoFilter::All

  get todosToView {
    case (todoFilter) {
      TodoFilter::All => todos
      TodoFilter::Active => Array.reject((t : Todo) { t.completed }, todos)
      TodoFilter::Completed => Array.reject((t : Todo) { !t.completed }, todos)
    }
  }

  get todosActive : Number {
    Array.size(todos) - todosCompleted
  }

  get todosCompleted : Number {
    Array.reduce(
      0,
      (acc : Number, t : Todo) : Number {
        if (t.completed) {
          acc + 1
        } else {
          acc
        }
      },
      todos)
  }

  fun loadTodosFromLocalStorage : Promise(Never, Void) {
    if (Array.isEmpty(todos)) {
      try {
        data =
          Storage.Local.get(LOCAL_STORAGE_KEY)

        object =
          Json.parse(data)
          |> Maybe.toResult("")

        todosFromStorage =
          decode object as Array(Todo)

        next { todos = todosFromStorage }
      } catch Storage.Error => error {
        next { todos = [] }
      } catch Object.Error => error {
        next { todos = [] }
      } catch String => error {
        next { todos = [] }
      }
    } else {
      next {  }
    }
  }

  fun saveTodosToLocalStorage (newTodos : Array(Todo)) : Promise(Never, Void) {
    try {
      (encode newTodos)
      |> Json.stringify()
      |> Storage.Local.set(LOCAL_STORAGE_KEY)

      next {  }
    } catch Storage.Error => error {
      next {  }
    }
  }

  fun setTodoFilter (newFilter : TodoFilter) : Promise(Never, Void) {
    next { todoFilter = newFilter }
  }

  fun nextId : Number {
    case (todos) {
      [] => 0
      => Array.reduce(0, (acc : Number, t : Todo) : Number { Math.max(acc, t.id) }, todos) + 1
    }
  }

  fun toggleAll : Promise(Never, Void) {
    sequence {
      newTodos =
        if (todosCompleted == Array.size(todos)) {
          Array.map((t : Todo) : Todo { { t | completed = false } }, todos)
        } else {
          Array.map((t : Todo) : Todo { { t | completed = true } }, todos)
        }

      saveTodosToLocalStorage(newTodos)
      next { todos = newTodos }
    }
  }

  fun clearCompleted : Promise(Never, Void) {
    sequence {
      newTodos =
        Array.reject((t : Todo) { t.completed }, todos)

      saveTodosToLocalStorage(newTodos)
      next { todos = newTodos }
    }
  }

  fun destroy (id : Number) : Promise(Never, Void) {
    sequence {
      newTodos =
        Array.reject((t : Todo) { t.id == id }, todos)

      saveTodosToLocalStorage(newTodos)
      next { todos = newTodos }
    }
  }

  fun updateTodo (newTodo : Todo) : Promise(Never, Void) {
    sequence {
      newTodos =
        Array.reduce(
          [],
          (acc : Array(Todo), oldTodo : Todo) : Array(Todo) {
            case (oldTodo.id) {
              newTodo.id => Array.push(newTodo, acc)
              => Array.push(oldTodo, acc)
            }
          },
          todos)

      saveTodosToLocalStorage(newTodos)

      next { todos = newTodos }
    }
  }

  fun addNewTodo (label : String) : Promise(Never, Void) {
    if (label == "") {
      Promise.never()
    } else {
      sequence {
        newTodos =
          Array.push(
            {
              label = label,
              completed = false,
              id = nextId()
            },
            todos)

        saveTodosToLocalStorage(newTodos)

        next { todos = newTodos }
      }
    }
  }
}

/* This is the static Sidebar content */
component Sidebar {
  fun render : Html {
    <aside class="learn">
      <header>
        <img
          src="https://github.com/mint-lang/mint/raw/master/src/assets/mint-logo.svg"
          alt="Mint logo"
          style="max-width:100%;"/>

        <span class="source-links">
          <h5>"Example"</h5>

          <a href="https://gitlab.com/greggreg/mint-todo-mvc">
            "Source"
          </a>
        </span>
      </header>

      <hr/>

      <blockquote class="quote speech-bubble">
        <p>"The programming language for writing single page applications."</p>

        <footer>
          <a href="http://mint-lang.org">
            "Mint"
          </a>
        </footer>
      </blockquote>

      <hr/>
      <h4>"Official Resources"</h4>

      <ul>
        <li>
          <a href="http://mint-lang.org/">
            "Mint homepage"
          </a>
        </li>
      </ul>

      <footer>
        <hr/>

        <em>
          "If you have other helpful links to share, or find any of the links above no longer work, please "

          <a href="https://gitlab.com/greggreg/mint-todo-mvc">
            "let us know "
          </a>

          "."
        </em>
      </footer>
    </aside>
  }
}

/* Application router */

routes {
  * {
    sequence {
      Todo.Store.loadTodosFromLocalStorage()
      Todo.Store.setTodoFilter(TodoFilter::All)
    }
  }

  /#/active {
    sequence {
      Todo.Store.loadTodosFromLocalStorage()
      Todo.Store.setTodoFilter(TodoFilter::Active)
    }
  }

  /#/completed {
    sequence {
      Todo.Store.loadTodosFromLocalStorage()
      Todo.Store.setTodoFilter(TodoFilter::Completed)
    }
  }
}
