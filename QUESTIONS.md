### Sending Events to a parent component without using a store

The `Main` component renders a list of `Todo.View`s. The `Todo.Views` have the event handlers
for when a todo should enter or exit editing. In order to propigate that state up to the `Main`
component, when rendering the `Todo.View` compenent I am passing in functions called `onEditStart`
and `onEditStop` as properties and then calling those functions as needed. The alternative I
could see would be to use a Store to manage the state but I felt the scope of this state wasn't
wide enough to need a global state manager.

Is this a good way to manage this state?

### Focusing Inputs

In order to focus an input I wrote the following javascript inside of a `componentDidMount` function:

```
`document.getElementById("todo-edit-input").focus()`
```

Is this the right way to do that?
