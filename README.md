# TodoMVC in Mint

Demo here: [http://mint-todo-mvc.greg.lol/](http://mint-todo-mvc.greg.lol/)

# Running Locally

- Intstall [Mint](https://mint-lang.org)
- Clone this repo
- `cd` into this repo
- run `mint start`
